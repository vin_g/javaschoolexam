package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
// Vinnikov Grigory vinnikov@inbox.ru
    private static int LINES_QUANTITY;
    private static int ELEMENTS_QUANTITY;

    public static int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException | OutOfMemoryError e)
        {
            throw new CannotBuildPyramidException();
        }
        //System.out.println("-------Implement your solution here inputNumbers:" + inputNumbers);
        // check if we can not Build Pyramid
        // check size
        boolean cannotBuildPyramidExceptionSize = checkCanBuildPyramidExceptionSize(inputNumbers);
        if (cannotBuildPyramidExceptionSize)
        {
//            System.out.println("********cannotBuildPyramidExceptionSize");
            throw new CannotBuildPyramidException();
        }
        // check digits, insurance
        boolean cannotBuildPyramidException = checkCanBuildPyramidException(inputNumbers);
        if (cannotBuildPyramidException)
        {
//            System.out.println("********cannotBuildPyramidException");
            throw new CannotBuildPyramidException();
        }

        //getQtyLinesInPyramid();
//        System.out.println("----LINES_QUANTITY:" + LINES_QUANTITY);
        int qtySubArrays = LINES_QUANTITY;
        int sizeSubArrays = LINES_QUANTITY + LINES_QUANTITY-1;
        //ELEMENTS_QUANTITY = sizeSubArrays;
//        System.out.println("----sizeSubArrays:" + sizeSubArrays);
        int[][] buildPyramidArr = new int[qtySubArrays][sizeSubArrays];

        // begin to fill in arrays for Pyramid building
        int zero = 0;
        int indexToIn = inputNumbers.size()-1;
        for (int i = qtySubArrays-1; i >= 0; i--) {
//            System.out.println("***69***:" + Arrays.deepToString(buildPyramidArr));
            int qtyZerosNeeded = LINES_QUANTITY - (i + 1);
//            System.out.println("------LINES_QUANTITY:" + LINES_QUANTITY);
//            System.out.println("------i+1:" + (i+1) );
//            System.out.println("------qtyZerosNeeded:" + qtyZerosNeeded);
            int qtyZerosDone = 0;
            int qtyInputedDigits = 0;
            // fill in arrays from the bottom, from the right side to the left side
            for (int j = sizeSubArrays-1; j >= -1; j--) {
//                System.out.println("----j:" + j);
                if (qtyZerosNeeded != qtyZerosDone && j >= 0)
                {
                    buildPyramidArr[i][j] = zero;
                    buildPyramidArr[i][sizeSubArrays-1-j] = zero;
                    qtyZerosDone++;
                } else {
//                    System.out.println("---indexToIn-:" + indexToIn);
                    if (indexToIn < 0 || j < 0) break;
                    else {
                        buildPyramidArr[i][j] = inputNumbers.get(indexToIn);
//                        System.out.println("***85***:" + Arrays.deepToString(buildPyramidArr));
                        indexToIn--;
                        qtyInputedDigits++;
                        if (qtyInputedDigits == i+1) break;
                        if (j > 0) j--;
                        if (j != 0) buildPyramidArr[i][j] = zero;
//                        System.out.println("***88***:" + Arrays.deepToString(buildPyramidArr));
                    }
                }
            }
        }
        return buildPyramidArr;
    }

    // check if it is impossible to build a pyramid in case of wrong size of inputted expression
    public static boolean checkCanBuildPyramidExceptionSize(List<Integer> inputNumbers)
    {
        boolean cannotBuildPyramidException1 = false;
        ELEMENTS_QUANTITY = inputNumbers.size();
//        System.out.println("---------164--ELEMENTS_QUANTITY:" + ELEMENTS_QUANTITY);
        int buffer = 0;
        for (int i = 0; i < ELEMENTS_QUANTITY; i++) {
            buffer += i;
//            System.out.println("---------164--buffer:" + buffer);
            if ( buffer < ELEMENTS_QUANTITY && (buffer + i) > ELEMENTS_QUANTITY )
            {
                cannotBuildPyramidException1 = true;
                break;
            } else if (buffer == ELEMENTS_QUANTITY)
            {
                LINES_QUANTITY = i;
                cannotBuildPyramidException1 = false;
                break;
            }
        }
//        System.out.println("---65--cannotBuildPyramidException1:" + cannotBuildPyramidException1);
        return cannotBuildPyramidException1;
    }

    // check if all digits are correct to build a pyramid
    public static boolean checkCanBuildPyramidException(List<Integer> inputNumbers) {
        for (int i = 0; i < inputNumbers.size(); i++) {
            if (inputNumbers.get(i) == null) // != i+1)
            {
                return true;
            }
        }
        return false;
    }
}
