package com.tsystems.javaschool.tasks.calculator.service;

import java.util.ArrayList;

public interface MathsDoings {
    ArrayList<String> getEachSymbol(String inputLine);
    String removeBraces(ArrayList<String> statementList, String statement);
    String doMathematics(String statementWithoutBraces);
}